# def MinMax(znach):
#     if(len(znach)>0):
#         min_=znach[0]
#         max_=znach[0]
#         sring= ""
#         for e in znach:
#             sring= sring+str(e)   # - Полученный массив результатов представить в виде одной строки;
#             if(e>max_):
#                 max_=e
#             if(min_>e):
#                 min_=e
#         print(f"Максимальное значение {max_} Минимальное значение {min_}")
#         #В найденной последовательности найти строку совпадающую с шаблоном;
#         print (sring)
#         n = sring.count(search)   
#         print(f"Найдено совпадений с шаблоном {n}")
#         return sring+'\n'
#     else:  
#         print("Нет значений")
#         return None

import math
answer = input("Загрузить значения из файла ? y/n ")   

function ={
'x':[],
'F':[],
'G':[],
'Y':[]
}

if (answer in ['N','n','нет','н']):
    f = open("data.txt", 'w')
    while (True):
        
        a = float(input("Введите a "))
        x_min= float(input("Введите границу x min "))
        x_max= float(input("Введите границу x max "))
        x_range= float(input("Введите размер шага "))

        print ("Значения:")
        x = x_min
        while (x < x_max):
            function['x'].append(x) 
            g_4isli= 7 * (-15*a*a + 22*a*x+5*x*x)
            g_znam = 4*a*a +7*a*x+ 3*x*x
            G=g_4isli/g_znam      
            function['G'].append(G)         
            print (f"X:{x:.6f} G:{G:.6f}")
            F = math.tan(4*a*a-3*a*x-7*x*x)
            function['F'].append(F) 
            print (f"X:{x:.6f} F:{F:.6f}")
            if ((-7*a*a+20*a*x+3*x*x+1)>-1 and (-7*a*a+20*a*x+3*x*x+1)<1 ):
                Y = math.acos(-7*a*a+20*a*x+3*x*x+1)
                function['Y'].append(Y) 
                print (f"X:{x:.6f} Y:{Y:.6f}")
                f.writelines(f"x:{x} G:{G} F:{F} Y:{Y}")
            else:
                print(F"X:{x:.6f} не_принадлежат_области_определения")
                f.writelines(f"x:{x} G:{G} F:{F} Y:не_принадлежат_области_определения\n")
            
            x +=x_range

        function['x'].clear()
        function['G'].clear()
        function['F'].clear()
        function['Y'].clear()

        answer = input("Произведём ещё вычесления ? y/n ")   
        if (answer in ['N','n','нет','н']):
            break
    f.close()
else:
    f = open("data.txt", 'r')
    for e in f:
        e_ = e.split()
        function['x'].append(float(e_[0][2:]))
        function['G'].append(float(e_[1][2:]))
        function['F'].append(float(e_[2][2:]))
        function['Y'].append((e_[3][2:]))

    for i in range(len(function['x'])):
        print ( F"X:{function['x'][i]:.6} G:{function['G'][i]:.6} F:{function['F'][i]:.6} Y:{function['Y'][i]}")  
    f.close()

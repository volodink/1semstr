import time 
class Vector2(): 
    def __init__ (self,x,y):
        self.x=x
        self.y=y
    def __add__(self, other):
        self.x+=other.x
        self.y+=other.y
        return self
    def __mul__(self,other):
        self.x*=other
        self.y*=other
        return self

def SAXPY (x,y,a):
    z = []
    for i in range(len(x)):
        z.append([])
        z[i]=  x[i]*a+y[i]

    return z

import timeit
f = open("time_kr_3.txt", 'w')
x = []
y = []
for ras in range(10000,1000000,1000):
    x.append(Vector2(ras,ras))
    y.append(Vector2(ras,ras))
    start_time = timeit.default_timer()
    z = SAXPY(x,y,ras)
    elapsed = timeit.default_timer() - start_time
    f.writelines(f"{elapsed:f}\n")
    print(elapsed)
f.close()
def getMatrixWithoutRowAndCol(matrix,row,col,newMatrix):
    size = len(matrix)
    offsetRow = 0 #Смещение индекса строки в матрице
    offsetCol = 0 #Смещение индекса столбца в матрице
    for i in range(size-1):
        #Пропустить row-ую строку
        if(i == row):
            offsetRow = 1 #Как только встретили строку, которую надо пропустить, делаем смещение для исходной матрицы   
        offsetCol = 0 #Обнулить смещение столбца
        for j in range (size-1):
            #Пропустить col-ый столбец
            if(j == col):
                offsetCol = 1 #Встретили нужный столбец, проускаем его смещением
            newMatrix[i][j] = matrix[i + offsetRow][j + offsetCol]

def matrixDet(matrix):
    size = len(matrix)
    det = 0
    degree = 1 # (-1)^(1+j) из формулы определителя

    #Условие выхода из рекурсии
    if(size == 1):
        return matrix[0][0]
    
    #Условие выхода из рекурсии
    elif(size == 2):
        return matrix[0][0]*matrix[1][1] - matrix[0][1]*matrix[1][0]
    
    else:

        #Матрица без строки и столбца
        newMatrix=[]
        for i in range(size-1):
            newMatrix.append([])
            for j in range(size-1):
               newMatrix[i].append(0)

        #Раскладываем по 0-ой строке, цикл бежит по столбцам
        for j in range(j,size):
            #Удалить из матрицы i-ю строку и j-ый столбец
            #Результат в newMatrix
            getMatrixWithoutRowAndCol(matrix, 0, j, newMatrix)

            #Рекурсивный вызов
            #По формуле: сумма по j, (-1)^(1+j) * matrix[0][j] * minor_j (это и есть сумма из формулы)
            #где minor_j - дополнительный минор элемента matrix[0][j]
            # (напомню, что минор это определитель матрицы без 0-ой строки и j-го столбца)
            det = det + (degree * matrix[0][j] * matrixDet(newMatrix))
            #"Накручиваем" степень множителя
            degree = -degree
        newMatrix.clear()
    return det


mas = [ [1,2, 3],
        [1,1, 1],
        [1,2,-1] ]
print(matrixDet(mas))
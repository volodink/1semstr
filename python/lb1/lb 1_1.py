import math
print("Программа для подсчёта площади треугольника по трём сторонам ")
A = float(input("Введите 1-ю сторону А "))
B = float(input("Введите 2-ю сторону B "))
C = float(input("Введите 3-ю сторону C "))
p = ((A+B+C)/2)
s = math.sqrt(p*(p - A)*(p - B)*(p - C))
print(f"площадь треугольника равна {s} ")
input()
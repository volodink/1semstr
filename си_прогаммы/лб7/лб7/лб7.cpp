#include "pch.h"
#include <iostream>
#include <math.h>
#include <stdio.h>

int main(void)
{
	setlocale(LC_ALL, "Russian");
	char temp='y';
	printf("Загрузить результаты из файла function.txt ? y/n");
	scanf_s("%c",&temp );
	bool loadRes = temp != 'n';

	struct
	{
		float G[255] = {};
		float F[255] = {};
		float Y[255] = {};
		float x[255] = {};
	}function;
	
	if (!loadRes)
	{
		float a, x_min, x_max, x_range;
		printf("Введите a, x min, x max,размер шага");
		scanf_s("%f" "%f" "%f" "%f", &a, &x_min, &x_max, &x_range);
		printf("a= %f, x min=%f, x max=%f,размер шага=%f \n", a, x_min, x_max, x_range);

		FILE *file;
		file = (FILE *)fopen("function.txt", "w+");
		char buf[10] = "";//
		int i = 0;
		for (float x = x_min; x < x_max; x += x_range)
		{
			float g_4isli, g_znam;
			g_4isli = 7 * (-15 * a*a + 22 * a*x + 5 * x*x);
			g_znam = 4 * a*a + 7 * a*x + 3 * x*x;
			function.G[i] = g_4isli / g_znam;
			function.F[i] = tan(4 * a*a - 3 * a*x - 7 * x*x);

			float Y_ = -7 * a*a + 20 * a*x + 3 * x*x + 1;
			if (Y_ > -1 and Y_ < 1)
			{
				function.Y[i] = cos(Y_);
				printf("x:%F G:%F F:%F Y:%F\n", x, function.G[i], function.F[i], function.Y[i]);
				fprintf(file, "%F %F %F %F\n",x, function.G[i], function.F[i], function.Y[i]);
			}
			else
			{
				printf("x:%F G:%F F:%F Y:Не определено\n", x, function.G[i], function.F[i]);
				fprintf(file, "%F %F %F Не_определено\n",x, function.G[i], function.F[i]);
			}
			i++;
		}
		fclose(file);
	}
	else
	{
		FILE *file;
		file = (FILE *)fopen("function.txt", "r");
		int n = 0;
		char string[50] = "";
		char buf_Y[] = "Не_определено";
		while (fgets(string, sizeof string, file))/*fscanf(file, "%F %F %F %s", &function.x[n], &function.G[n], &function.F[n], &buf)*/
		{				 
			if (sscanf(string, "%F %F %F %s", &function.x[n], &function.G[n], &function.F[n], &buf_Y) >= 1) 
			{
				if (buf_Y[0] >= '0' && buf_Y[0] <= '9') //проверяю на число
				{
					function.Y[n] = strtof(buf_Y, NULL);
					printf("x:%F G:%F F:%F Y:%F \n", function.x[n], function.G[n], function.F[n], function.Y[n]);
				}
				else
					printf("x:%F G:%F F:%F Y:%s \n", function.x[n], function.G[n], function.F[n], buf_Y);
				n++;
			}
		}
		fclose(file);
	}
	return 0;
}

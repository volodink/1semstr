// лб8.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include <math.h>
#include <time.h>
#include <omp.h>
struct point 
{
	float x;
	float y;
	char name[8]=" ";
};


point getRandomPoint()
{
	float x_ = rand() % 100; // 0 -99
	float y_ = rand() % 100; // 0 -99
	return point{ x_,y_ };
}

float dist(point a, point b)
{
	return sqrt((a.x - b.x) + (a.y - b.y));
}



int search(point point_w, point* arrPoint, int  arrLenght,float r)
{
	int n = 0; //количество точек в радиусе	
	for (int i = 0; i < arrLenght; i++)
	{
		if (dist(point_w, arrPoint[i]) < r)
		{
			n++;
		}
	}
	return n;


}

int main()
{
	setlocale(LC_ALL, "Russian");

	struct point arrPoint[5000]; // генерация точек
	for (int i = 0; i < 5000; i++)	
	{
		arrPoint[i] = getRandomPoint();
		//printf("x:%F y:%F\n", arrPoint[i].x, arrPoint[i].y);
	}

	printf("Введите любую точку номер точки или её кординаты(точка будет добавлена)\n");
	float x= 0;
	float y= 0;
	float r= 0;
	point point_w;
	if (scanf_s("%f,%f", &x, &y) == 1) //
	{
		point_w = arrPoint[(int)x];
	}
	else
	{
		point_w = { x,y };
	}
	printf("Введите радиус поиска\n");
	scanf_s("%f", &r);

	FILE *file;
	file = (FILE *)fopen("Time.txt", "w+");
	for (int  i = 10; i < 5000; i++)
	{
		double start = omp_get_wtime();
		search(point_w, arrPoint, i, r);
		double end = omp_get_wtime();

		printf("Время поиска%lfмс\n", end-start);
		fprintf(file, "%lf\n", end - start);

	}
	fclose(file);
}



